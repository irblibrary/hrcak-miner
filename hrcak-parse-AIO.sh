#!/bin/bash

KEYWORDS='data|podaci|supplement|podataka|podatke|podatci|pohran|underlying|deposit|prikupljen|arhiv'

for FILENAME in `ls -1 upute/* |egrep -i '*.(pdf|doc|docx)$'` 
do
	EXTENSION="${FILENAME##*.}"
	echo "<p>"
	echo $FILENAME
	echo "<br>"
	echo "----------"
	echo "<br>"
	case $EXTENSION in
		pdf)
			pdf2txt $FILENAME | egrep -B 2 -C 2 -i $KEYWORDS | perl -p -e "s/($KEYWORDS)/<span style=\"color:red;font-weight:bold\">\1<\/span>/gi;" -e "s/\\n/<br>/g;" \
				|| true
			;;
		doc)
			catdoc $FILENAME | egrep -B 2 -C 2 -i $KEYWORDS | perl -p -e "s/($KEYWORDS)/<span style=\"color:red;font-weight:bold\">\1<\/span>/gi;" -e "s/\\n/<br>/g;" \
				|| true
			;;
		docx)
			docx2txt $FILENAME | egrep -B 2 -C 2 -i $KEYWORDS | perl -p -e "s/($KEYWORDS)/<span style=\"color:red;font-weight:bold\">\1<\/span>/gi;" -e "s/\\n/<br>/g;" \
				|| true
			;;
		*)
	esac


	echo "<br>"
	echo "----------"
	echo "</p>"
	echo ""
	echo ""
done
