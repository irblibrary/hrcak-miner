import re
import time
import pprint
import codecs
import mechanize
import urllib.request
import logging
import random
from bs4 import BeautifulSoup
import sys  

#reload(sys)  
#sys.setdefaultencoding('utf8')

logger = logging.getLogger("mechanize")
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)

#
# FUNC
#
def parse ( html_doc ):

    soup = BeautifulSoup(html_doc, features="html5lib")
    
    journal_line = {} 
	
    # REGEX PART
	
    jdet = soup.select('td#sadrzaj')
    journal_det =   jdet[0]
	
    # TITLE
    r_title = re.search("<h1 class=\"naslov\">(.*?)</h1>", str(journal_det), re.DOTALL)
    title = r_title.groups()[0]

    # ISSN PRINT
    r_issn_p = re.search("ISSN ([0-9xX]{4}-[0-9xX]{4}) \(Tisak\)", str(journal_det), re.DOTALL)
    if r_issn_p:
        issn_p = r_issn_p.groups()[0]
    else:
        issn_p = 'NULL'
	
    # ISSN E 
    r_issn_e = re.search("ISSN ([0-9xX]{4}-[0-9xX]{4}) \(Online\)", str(journal_det), re.DOTALL)
    if r_issn_e:
        issn_e = r_issn_e.groups()[0]
    else:
        issn_e = 'NULL'

    # URL
    r_url = re.search("<td>Url:<\/td>.*?<a.*?>(.*?)<\/a>", str(journal_det), re.DOTALL)
    if r_url:
        url = r_url.groups()[0]
    else:
        url = 'NULL'

    # PODRUCJA
    r_podrucja = re.search("<td class=\"no-wrap\">Područja pokrivanja: <\/td>.*?<td>(.*?)<\/td>", str(journal_det), re.DOTALL)
    if r_podrucja:
        podrucja = r_podrucja.groups()[0]
    else:
        podrucja = 'NULL'

    # PRVA GOD. IZLAZENJA
    r_godina = re.search("<p>Prva godina izlaženja: ([0-9]{4})</p>", str(journal_det), re.DOTALL)
    if r_godina:
        godina = r_godina.groups()[0]
    else:
        godina = 'NULL'


    # RETURN
    journal_line['tsv'] = '|' + title + "|\t|" + issn_p + "|\t|" + issn_e + "|\t|" + url + "|\t|" + podrucja + "|\t|" + godina + "|"

    journal_line['tsv'] = journal_line['tsv'].replace('\n', ' ').replace('\r', '')

    journal_line['fname'] = issn_p + "__-__" + issn_e + "__-__"

    return journal_line;

#
# MAIN
#

f = codecs.open('out/hrcak_journals.tsv', encoding='utf-8', mode='w+')

br = mechanize.Browser()
br.set_handle_robots(False)
br.set_handle_refresh(False)
br.addheaders = [('User-agent', 'Firefox')]

br1 = mechanize.Browser()
br1.set_handle_robots(False)
br1.set_handle_refresh(False)
br1.addheaders = [('User-agent', 'Firefox')]

br2 = mechanize.Browser()
br2.set_handle_robots(False)
br2.set_handle_refresh(False)
br2.addheaders = [('User-agent', 'Firefox')]


urlhost = 'http://hrcak'


url = ('file:///HRCAK-miner/journals.list')

response = br1.open(url)
br1._factory.is_html = True

journals = br1.links(url_regex='.')

for journal in journals:

    time.sleep(random.randint(1,3))

    docs = ''

    print (journal.url)

    response = br2.open(urlhost + journal.url)

    html_doc = response.read()
    journal_line = parse( html_doc )
    print (journal_line['tsv'])

    links = br2.links(url_regex='upute')
	
    for link in links:
	
        url_fname = link.url

        filename = "upute/" + journal_line['fname'] + url_fname[7:]

        upute_url = urlhost + link.url
        
        urllib.request.urlretrieve(upute_url, filename)
        
        print ("\t" + filename)

        docs = docs + link.url + ';'

    journal_line['tsv'] = journal_line['tsv'] + "\t|" + docs + "|" 

    print (journal_line['tsv'])

    journal_line['tsv'] = journal_line['tsv'] + "\n"

    f.write(journal_line['tsv'])

f.close()
